#include <stdio.h>
#include <stdlib.h>

enum StateOf{error, process, end};
enum TypeSymbol{open, close, other};

    // 40      // Круглая скобка
    // 91      // Квадратная скобка
    // 123     // Фигурная скобка
    // 41      // Круглая скобка
    // 93      // Квадратная скобка
    // 125     // Фигурная скобка

int Symbol[6] = {40, 91, 123, 41, 93, 125};

enum StateOf SearchSymbolInsert(char* string, int* posUpLevel, int idSymbolClose);
enum TypeSymbol DeterminationType(int symbol);
enum StateOf SearchSymbol(char* string);
int SearchIDSymbol(int symbol);
int InversSymbol(int symbol);

int main(){
    char string[20];
    enum StateOf status;

        printf("Enter the string: ");
        fgets(string, 20, stdin);
        status = SearchSymbol(string);
        printf("STATUS: %s\n", (status == process) ? "GOOD" : "ERROR");

    return 0;
}

enum StateOf SearchSymbol(char* string){
    enum StateOf status = process;
    enum TypeSymbol typeSymbol;
    int symbol;
    int i = 0;

    while(status == process && string[i] != '\n'){
        symbol = string[i];
        typeSymbol = DeterminationType(symbol);
        // printf("%s \n", (typeSymbol == other) ? "other" : ((typeSymbol == open) ? "open" : "close"));

        if(typeSymbol == open)
            status = (SearchSymbolInsert(string, &i, InversSymbol(symbol)) == end) ? process : error;
        else if (typeSymbol == close) status = error;

        i++;
    }

    return status;
}

enum StateOf SearchSymbolInsert(char* string, int* posUpLevel, int symbolCloseUpLevel){
    enum StateOf status = process;
    enum TypeSymbol typeSymbol;
    int i = *posUpLevel + 1;
    int symbol;

    while(status == process && string[i] != '\n'){
        symbol = string[i];
        typeSymbol = DeterminationType(symbol);
        // printf("%s \n", (typeSymbol == other) ? "other" : ((typeSymbol == open) ? "open" : "close"));

        if(typeSymbol == open) {
            status = (SearchSymbolInsert(string, &i, InversSymbol(symbol)) == end) ? process : error;
        }
        else if (typeSymbol == close)
            if (symbol == symbolCloseUpLevel) status = end;
            else status = error;

        if (status == end) *posUpLevel = i;
        else i++;
    }

    return status;
}

enum TypeSymbol DeterminationType(int symbol){
    int id = SearchIDSymbol(symbol);
    // printf("ID: %i SYMBOL: %c ", id, symbol);
    return (id == 6) ? other : ((id <= 2) ? open : close);
}

int InversSymbol(int symbol){
    int id = SearchIDSymbol(symbol);
    return (id <= 2) ? Symbol[id + 3] : Symbol[id - 3];
}

int SearchIDSymbol(int symbol){
    int i, id = 6;

    for(i = 0; i < id; i++)
        if (Symbol[i] == symbol) id = i;

    return id;
}
