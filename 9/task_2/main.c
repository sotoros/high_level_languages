#include <stdio.h>
#include <math.h>
#include <conio.h>

int Calc(int *a, int n);

int main(){
    const int n = 10;
    int a[n];

    Calc(a, n);

    return 0;
}

int Calc(int *a, int n){
    int i, imax, imin, ibeg, iend, counter;

    for(i = 0; i < n; i++){
        printf("a[%i]: ", i);
        scanf("%i", &a[i]);
    }

    for(i = imax = imin = 0; i < n; i++){
        if(a[i] > a[imax]) imax = i;
        if(a[i] < a[imin]) imin = i;
    }

    printf("max = %d\t min = %d\t \n", a[imax],a[imin]);

    ibeg = imax < imin ? imax : imin;
    iend = imax < imin ? imin : imax;

    printf("ibeg=%d\t iend=%d\n",ibeg,iend);

    for(counter = 0, i = ibeg + 1; i < iend; i++)
        if(a[i] > 0) counter++;

    printf("The number of positive elements = %d\n",counter);

    _getch();

    return 0;
}
