#include <stdio.h>
#include <conio.h>

int main(){
// char, short, int, long, float, double, long double
    printf("CHAR: \t\t%i BYTE\n", sizeof(char));
    printf("SHORT: \t\t%i BYTE\n", sizeof(short));
    printf("INT: \t\t%i BYTE\n", sizeof(int));
    printf("LONG: \t\t%i BYTE\n", sizeof(long));
    printf("FLOAT: \t\t%i BYTE\n", sizeof(float));
    printf("DOUBLE: \t%i BYTE\n", sizeof(double));
    printf("LONG DOUBLE: \t%i BYTE\n", sizeof(long double));

    _getch();

    return 0;
}