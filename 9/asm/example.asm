format mz
       entry main:begin

segment main

	begin:

		; DS - Data Segment
		; CS - Code Segment

		mov	ax, data_segment
		mov	ds, ax

		; ����� ������
		mov	dx, start_msg
		mov	ah, 09h
		int	21h

		; ���� ������
		mov    dx, bufferSize
		call   input

		; Check on free string
		test	al, al
			jz  exit

		movzx	bx, byte [inputLength]
		mov	dword [ds:buffer + bx], '$'

		mov	si, 0
		call	checkSymbol



		; ����� ������
		call	newLine
		mov	dx, buffer
		call	output


	exit:
		call	getch
		mov	ax, 4c00h
		int	21h

;======================================================
;			���������
;======================================================

	getch:
		mov	ah,01h
		int	21h
	ret

	output:
		mov	ah, 09h
		int	21h
	ret

	input:
		mov	ah, 0Ah
		int	21h
	ret

	newLine:
		mov	dx, newline_string
		call	output
	ret

	checkSymbol:
		cmp	byte [ds:buffer + si], closeSymbol
			je   closeEvent
		cmp	byte [ds:buffer + si], 40d
			je   innerEvent
		cmp	byte [ds:buffer + si], 91d
			je   innerEvent
		cmp	byte [ds:buffer + si], 123d
			je   innerEvent
		cmp	byte [ds:buffer + si], 41d
			je   errorEvent
		cmp	byte [ds:buffer + si], 93d
			je   errorEvent
		cmp	byte [ds:buffer + si], 125d
			je   errorEvent
	ret

;======================================================
;			����������
;======================================================

segment data_segment

	start_msg:		db 'Enter string: $'
	bufferSize:		db 21			; 20 char + RETURN
	inputLength:		db ?			; number of read charaxters
	buffer: 		db 24 dup(9)		; buffer
	newline_string: 	db 13, 10, '$'		; new line
	closeSymbol:		db 0			; close symbol