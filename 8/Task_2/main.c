#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

int SearchPositiveNumber(float *array, int size, int startPosition);
int TransformationArray(float *array, int size);
int FillingArrayMark(float *arrray, int size);
int CountNotAllowed(float *array, int size);
int MaxAbsElement(float *array, int size);
int FillingArray(float *array, int size);
float SummArrayElements(float *array, int size);
float RandomMarkNumber();
float RandomRealNumber();

int main(){
    int numberOperation;
    int size;               // Size of array
    int i;
    float *array;           // Pointer to first element of array

    srand(time(0)); // Initialize random number generator

    // Create array of N size from real number
    printf("Please enter the size of the array: ");
    scanf("%i", &size);

    array = (float*)malloc(size * sizeof(float));

    printf("Please enter the number operation: ");
    scanf("%i", &numberOperation);

    switch(numberOperation){
        case 1:
            FillingArray(array, size);
            MaxAbsElement(array, size);
            break;
        case 2:
            FillingArray(array, size);
            SummArrayElements(array, size);
            break;
        case 3:
            FillingArray(array, size);
            TransformationArray(array, size);
            break;
        case 4:
            FillingArrayMark(array, size);
            CountNotAllowed(array, size);
            break;
        default:
            break;                    
    }

    free(array);

    _getch();

    return 0;
}

int MaxAbsElement(float *array, int size){
    int position = 0;
    int i;

    for(i = 0; i < size; i++)
        if(abs(array[i]) > abs(array[position]))
            position = i;

    printf("MAX ABS VALUE: %f\n", array[position]);

    return position;
}

float SummArrayElements(float *array, int size){
    float summ;
    int i;

    int posStart = SearchPositiveNumber(array, size, 0) + 1;
    int posEnd = SearchPositiveNumber(array, size, posStart);

    for(i = posStart; i < posEnd; i++)
        summ += array[i];

    printf("SUMM: %f\n", summ);

    return summ;
}

int SearchPositiveNumber(float *array, int size, int startPosition){
    int i, position;

    for(i = startPosition; i < size; i++){
        if(array[i] > 0){
            position = i;
            break;    
        }
    }

    return position;
}

int TransformationArray(float *array, int size){
    int posZero = (size - 1);
    int i; 

    for(i = 0; i < posZero; i++){
        while(array[i] == 0){
            if(array[posZero] != 0){
                array[i] = array[posZero];
                array[posZero] = 0; 
            }
            posZero--;
        }
    }

    printf("RESULT: \n");

    for(i = 0; i < size; i++)
        printf("%f\n", array[i]);

    return 0;
}

int FillingArray(float *array, int size){
    int i;

    // Filling an array
    for(i = 0; i < size; i++)
        array[i] = RandomRealNumber();

    // Output values of array
    for(i = 0; i < size; i++)
        printf("%f\n", array[i]);

    return 0;
}

int FillingArrayMark(float *array, int size){
    int i;

    // Filling an array
    for(i = 0; i < size; i++)
        array[i] = RandomMarkNumber();
    
    // Output values of array
    for(i = 0; i < size; i++)
        printf("%f\n", array[i]);

    return 0;
}

int CountNotAllowed(float *array, int size){
    int count = 0;
    int i;

    for(i = 0; i < size; i++)
        if(array[i] == 2)
            count++;

    printf("Number of not allowed: %i\n", count);

    return count;
}

float RandomRealNumber(){
    float number;

    if(rand()%10 == 0) number = 0;
    else {
        float integerPart = (float)(rand() - (RAND_MAX / 2));
        float realPart = (float)rand()/100000;
        number = integerPart + realPart;
    }

    return number;
}

float RandomMarkNumber(){
    return (float)(rand() % 4 + 2);
}
